package com.example.movies.service;

import com.example.movies.dto.MovieDto;
import com.example.movies.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MovieServiceImpl implements MovieService{
    

    final MovieRepository movieRepository;
    @Autowired
    public MovieServiceImpl(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }


    @Override
    public Map<String, List<MovieDto>> getMovies() {
        Map<String, List<MovieDto>> movies = new HashMap<>();
        movies.put("movies", movieRepository.getMovieList());
        return movies;
    }
}

