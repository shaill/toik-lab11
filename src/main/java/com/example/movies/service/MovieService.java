package com.example.movies.service;

import com.example.movies.dto.MovieDto;

import java.util.List;
import java.util.Map;

public interface MovieService {
    Map<String, List<MovieDto>> getMovies();
}
