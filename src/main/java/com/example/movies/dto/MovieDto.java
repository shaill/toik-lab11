package com.example.movies.dto;

import lombok.*;

import java.io.Serializable;

@Data
@Builder
public class MovieDto implements Serializable {

    private int movieId;
    private String title;
    private int year;
    private String image;


}
